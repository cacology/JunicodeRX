all : roman italic bold-italic bold

roman : JunicodeRX.ttf

italic : JunicodeRX-Italic.ttf

bold-italic : JunicodeRX-BoldItalic.ttf

bold : JunicodeRX-Bold.ttf

%-nohint.ttf : src/%.sfd
	fontforge -script util/convert.pe $<
	mv src/$*.ttf $@

%.ttf : %-nohint.ttf
	ttfautohint -f latn --hinting-range-min=20 --hinting-range-max=150 $< $@

clean :
	rm -f *.ttf
