JunicodeRX
===
Junicode with Restoration Era Typographic Extensions

## Overview
This is a modification of Peter Baker's
[Junicode](http://junicode.sourceforge.net) with Restoration-era
features.  It's based on Version 1.001 from 2017, but because I can't
get the UFO build working quite right, it's not a fork.  If you want
to help make the fork work, the version is here
[https://github.com/cacology/junicode-restoration](https://github.com/cacology/junicode-restoration).
This version builds on the FontForge SFD, see Makefile for details,
but there are known problems for this approach--notably, that the
quadratic curves in FontForge are cubic internally and so migrate by
fractions of points on each save.

## Demos

[Italic demo](http://slab.space/~jpsa/fk-JIRX-demo/)

[Roman demo](http://slab.space/~jpsa/fk-JRX-demo/)


## TODO
- [x] modify glyph for ſ in Roman and bold
- [x] adjust ligatureless kerning in roman and bold
- [x] setup contextual alternatives for roman and bold
- [x] rename Junicode [roman]
- [x] rename Junicode-Italic, Junicode-Bold when altered
- [ ] write rationale and examples
- [x] write testing and examples
- [ ] identify other characters to import

### Italic work
- [x] adjust ligatureless kerning in italic, it-bold
- [x] make italic version of ſ, it-bold
- [x] setup contextual alternatives for italic, it-bold

### Dissertation Additions
For each, draw the character, check the kerning, look at default
substitutions for italic- ignore it-bold for the time being.

- [x] \def\swashA{{\itjunicode A}}
- [x] \def\swashB{{\itgaramond B}}
- [x] \def\swashC{{\itgaramond C}}
- [x] \def\swashD{{\itgaramond D}}
- [x] \def\swashG{{\itgaramond G}}
- [x] \def\swashJ{{\itjunicode J}}
- [x] \def\swashK{{\itgaramond K}}
- [x] \def\swashk{\getnamedglyphdirect{name:HoeflerText-Italic}{k.swash.lf}}
- [x] \def\swashM{{\itgaramond M}}
- [x] \def\swashN{{\itgaramond N}}
- [x] \def\swashP{{\itgaramond P}}
- [x] \def\swashQ{{\itjunicode Q}}
- [x] \def\swashR{{\itgaramond R}}
- [x] \def\leftswashR{{\itwarnock R}}
- [x] \def\rightswashR{{\tfb \kern-0.08em \getnamedglyphdirect{name:Apple-Chancery}{R.small.1}}}
- [x] \def\swashT{{\itgaramond T}}
- [x] \def\swashv{\getnamedglyphdirect{name:HoeflerText-Italic}{v.swash.li}}
- [x] \def\swashV{{\itgaramond V}}
- [x] \def\swashY{{\itgaramond Y}}
- [x] \def\swashz{\getnamedglyphdirect{name:HoeflerText-Italic}{z.swash.lf}}

- [x] \def\ligas{\getnamedglyphdirect{name:HoeflerText-Italic}{a_s}}
- [x] \def\ligis{\getnamedglyphdirect{name:HoeflerText-Italic}{i_s}}
- [x] \def\ligll{\getnamedglyphdirect{name:HoeflerText-Italic}{l_l}}
- [x] \def\ligus{\getnamedglyphdirect{name:HoeflerText-Italic}{u_s}}



## References
* [http://junicode.sourceforge.net](http://junicode.sourceforge.net)
